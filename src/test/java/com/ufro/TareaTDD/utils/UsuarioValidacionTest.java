package com.ufro.TareaTDD.utils;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class UsuarioValidacionTest {


    UsuarioValidacion usuarioValidacion = new UsuarioValidacion();


    @BeforeAll
    static void beforeAll() {
    }

    @Test
    void testComprobarNombre() {
        String nombre = "Nombre";
        String nombreSinMayus = "nom23bre";

        boolean contieneDigitos = usuarioValidacion.comprobarPalabraContieneDigitos(nombre);
        boolean nombreComienzaMayus = usuarioValidacion.comprobarPalabraComienzaConMayuscula(nombre);
        Assertions.assertFalse(contieneDigitos, "El nombre esta correcto");
        Assertions.assertTrue(nombreComienzaMayus, "el nombre esta correcto");


        contieneDigitos = usuarioValidacion.comprobarPalabraContieneDigitos(nombreSinMayus);
        nombreComienzaMayus = usuarioValidacion.comprobarPalabraComienzaConMayuscula(nombreSinMayus);

        Assertions.assertTrue(contieneDigitos, "El nombre contiene al menos un Digito");
        Assertions.assertFalse(nombreComienzaMayus, "El nombre no comienza con mayusculas");

    }


    @Test
    void testComprobarApellido() {
        String apellido = "Apellido";
        String apellidoSinMayus = "apellido2";

        boolean contieneDigitos = usuarioValidacion.comprobarPalabraContieneDigitos(apellido);
        boolean nombreComienzaMayus = usuarioValidacion.comprobarPalabraComienzaConMayuscula(apellido);
        Assertions.assertFalse(contieneDigitos, "El nombre esta correcto");
        Assertions.assertTrue(nombreComienzaMayus, "el nombre esta correcto");


        contieneDigitos = usuarioValidacion.comprobarPalabraContieneDigitos(apellidoSinMayus);
        nombreComienzaMayus = usuarioValidacion.comprobarPalabraComienzaConMayuscula(apellidoSinMayus);

        Assertions.assertTrue(contieneDigitos, "El apellido contiene al menos un Digito");
        Assertions.assertFalse(nombreComienzaMayus, "El apellido no comienza con mayusculas");
    }


    @Test
    void testComprobarRut() {
        String rut = "10.607.314-7";
        String rutIncorrecto = "23ola0.607.314-7";


        boolean comprobarRut = usuarioValidacion.comprobarRut(rut);
        boolean contieneLetra = usuarioValidacion.contieneLetra(rut);

        Assertions.assertFalse(contieneLetra, "el rut no contiene letras");
        Assertions.assertTrue(comprobarRut, "El rut es correcto");


        comprobarRut = usuarioValidacion.comprobarRut(rutIncorrecto);
        contieneLetra = usuarioValidacion.contieneLetra(rutIncorrecto);

        Assertions.assertTrue(contieneLetra, "el rut contiene letras");
        Assertions.assertFalse(comprobarRut, "El rut es incorrecto");

    }


    @Test
    void testComprobarNumTelefono() {
        String numTelefono = "+56 9 3969 2069";
        String numTelefonoIncorrecto = "-56 9 396asd9 2069";

        numTelefono = numTelefono.replace(" ", "");
        boolean comienzaConPlus = usuarioValidacion.comienzaConSignoSumaYFormatoEstandar(numTelefono);
        boolean contieneLetra = usuarioValidacion.comprobarPalabraContieneLetras(numTelefono);
        boolean tamañoDeseado = usuarioValidacion.comprobarTamanoNumeroTelefono(numTelefono);


        Assertions.assertFalse(contieneLetra, "correcto");
        Assertions.assertTrue(comienzaConPlus, "correcto");
        Assertions.assertTrue(tamañoDeseado, "correcto");


        comienzaConPlus = usuarioValidacion.comienzaConSignoSumaYFormatoEstandar(numTelefonoIncorrecto);
        contieneLetra = usuarioValidacion.comprobarPalabraContieneLetras(numTelefonoIncorrecto);
        tamañoDeseado = usuarioValidacion.comprobarTamanoNumeroTelefono(numTelefonoIncorrecto);

        Assertions.assertTrue(contieneLetra, "Numero telefonico contiene letras");
        Assertions.assertFalse(comienzaConPlus, "No comienza con el formato Estandar");
        Assertions.assertFalse(tamañoDeseado, "Incorrecto");
    }

    @Test
    void testComprobarEdad() {
        String edad = "@23";
        boolean contieneLetrasOSimbolos = usuarioValidacion.comprobarEdadContienePalabrasOSimbolos(edad);
        Assertions.assertTrue(contieneLetrasOSimbolos, "Contiene letras o símbolos");
    }


    @AfterAll
    static void afterAll() {

    }
}
