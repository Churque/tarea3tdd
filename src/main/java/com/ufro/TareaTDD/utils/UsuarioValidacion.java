package com.ufro.TareaTDD.utils;

import com.ufro.TareaTDD.DTO.Usuario;
import org.springframework.stereotype.Component;

@Component
public class UsuarioValidacion {


    public boolean comprobarRut(String rut) {
        boolean validacion = false;
        try {
            rut = rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (Exception ignored) {

        }
        return validacion;
    }

    public String retornarMensajeComprobacionRut(String rut) {

        //comprueba si el rut es valido
        String mensaje = "invalido: ";
        if (rut != null) {
            boolean validarRut = comprobarRut(rut);
            boolean contieneLetra = contieneLetra(rut);


            if (validarRut && !contieneLetra) {
                return "rut valido";
            } else if (contieneLetra) {
                mensaje += "rut contiene letras,";
            } else {
                return mensaje;
            }
        } else {
            mensaje += "rut no ha sido ingresado";
        }

        return mensaje;
    }

    public String retornarMensajeComprobacionTelefono(String numTelefono) {
        String mensaje = "invalido: ";

        //comprueba que el numero cumpla el formato +569
        //comprueba que no contenga letras, solo numeros
        // comprueba que cumpla el tamaño estandar de los numeros telefonicos
        if (numTelefono != null) {
            boolean comienzaConPlus = comienzaConSignoSumaYFormatoEstandar(numTelefono);
            boolean contieneLetra = comprobarPalabraContieneLetras(numTelefono);
            boolean tamañoDeseado = comprobarTamanoNumeroTelefono(numTelefono);

            if (!comienzaConPlus) {
                mensaje += "no comienza con formato estandar ";
            }
            if (contieneLetra) {
                mensaje += "contiene letras, ";
            }
            if (!tamañoDeseado) {
                mensaje += "no cumple el tamaño estandar, ";
            }

            if (comienzaConPlus && !contieneLetra && tamañoDeseado) {
                mensaje = "valido";
            }
            return mensaje;
        } else {
            mensaje += "numTelefono nulo";
            return mensaje;
        }
    }


    public String retornarMensajeComprobacionNombre(String nombre) {
        //comprueba que el nombre comienza con una Mayuscula y no contenga digitos

        String mensaje = "invalido: ";
        if (nombre != null) {
            boolean contieneDigitos = comprobarPalabraContieneDigitos(nombre);
            boolean comienzaConMayuscula = comprobarPalabraComienzaConMayuscula(nombre);
            boolean valido = false;


            if (contieneDigitos) {
                mensaje += "contiene digitos, ";
            }
            if (!comienzaConMayuscula) {
                mensaje += "no comienza con mayusculas, ";
            }

            if (!contieneDigitos && comienzaConMayuscula) {
                valido = true;
            }

            if (valido) {
                mensaje = "valido ";
            }
            return mensaje;
        } else {
            mensaje += "No ha sido ingresado";
            return mensaje;
        }
    }

    public boolean contieneLetra(String palabraARevisar) {
        boolean contieneLetra = false;

        for (int i = 0; i < palabraARevisar.length(); i++) {
            if (Character.isLetter(palabraARevisar.charAt(i))) {
                contieneLetra = true;
            }
        }

        return contieneLetra;
    }

    public boolean comprobarPalabraComienzaConMayuscula(String palabra) {
        return Character.isUpperCase(palabra.charAt(0));
    }

    public boolean comprobarPalabraContieneDigitos(String palabra) {
        boolean contieneDigitos = false;

        for (int i = 0; i < palabra.length(); i++) {
            if (Character.isDigit(palabra.charAt(i))) {
                contieneDigitos = true;
                break;
            }
        }

        return contieneDigitos;
    }

    public boolean comprobarPalabraContieneLetras(String palabra) {
        boolean contieneDigitos = false;

        for (int i = 0; i < palabra.length(); i++) {
            if (Character.isLetter(palabra.charAt(i))) {
                contieneDigitos = true;
                break;
            }
        }

        return contieneDigitos;
    }

    public boolean comienzaConSignoSumaYFormatoEstandar(String numeroTelefono) {
        return numeroTelefono.startsWith("+569");
    }

    public boolean comprobarTamanoNumeroTelefono(String numeroTelefono) {
        return numeroTelefono.length() == 12;
    }

    public boolean comprobarEdadContienePalabrasOSimbolos(String edad) {
        return edad.matches(".*\\D.*");
    }

    public String retornarComprobacionEdad(Integer edad) {
        //comprueba que edad solo contenga numeros
        String mensaje = "invalido: ";

        if (edad != null) {
            String valorEdad = String.valueOf(edad);
            if (comprobarEdadContienePalabrasOSimbolos(valorEdad)) {
                mensaje += "edad invalida \n";
            } else {
                mensaje = "edad valida \n ";
            }
        } else {
            mensaje += "edad no ingresada";
        }
        return mensaje;
    }
}
