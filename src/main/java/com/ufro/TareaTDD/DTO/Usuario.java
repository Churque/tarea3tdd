package com.ufro.TareaTDD.DTO;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor()
public class Usuario {


    String nombre;
    String apellido_Paterno;
    String apellido_Materno;
    String rut;
    String numero_Telefonico;
    Integer edad;


}
