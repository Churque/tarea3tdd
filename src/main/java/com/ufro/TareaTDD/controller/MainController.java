package com.ufro.TareaTDD.controller;


import com.ufro.TareaTDD.DTO.Usuario;
import com.ufro.TareaTDD.utils.UsuarioValidacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class MainController {

    @Autowired
    UsuarioValidacion usuarioValidacion;

    @PostMapping("/crearUsuario")
    public String crearUsuario(@RequestBody Usuario usuario) {
        StringBuilder stringBuilder = new StringBuilder();
        boolean todasLasValidacionesExitosas = true;

        stringBuilder.append("Nombre: ").append(usuarioValidacion.retornarMensajeComprobacionNombre(usuario.getNombre())).append("\n");
        stringBuilder.append("apellido paterno: ").append(usuarioValidacion.retornarMensajeComprobacionNombre(usuario.getApellido_Paterno())).append("\n");
        stringBuilder.append("apellido materno: ").append(usuarioValidacion.retornarMensajeComprobacionNombre(usuario.getApellido_Materno())).append("\n");
        stringBuilder.append("rut: ").append(usuarioValidacion.retornarMensajeComprobacionRut(usuario.getRut())).append("\n");
        stringBuilder.append("telefono: ").append(usuarioValidacion.retornarMensajeComprobacionTelefono(usuario.getNumero_Telefonico())).append("\n");
        stringBuilder.append("edad: ").append(usuarioValidacion.retornarComprobacionEdad(usuario.getEdad())).append("\n");

        if (stringBuilder.toString().contains("invalido")) {
            todasLasValidacionesExitosas = false;
        }

        if (todasLasValidacionesExitosas) {
            return "Usuario creado correctamente";
        } else {
            return stringBuilder.toString();
        }
    }
}
